FROM psono-docker.jfrog.io/nginx:alpine

LABEL maintainer="Sascha Pfeiffer <sascha.pfeiffer@psono.com>"

RUN apk upgrade --no-cache \
        && apk add --no-cache curl \
        && apk add --update npm

COPY ./package.json package.json
COPY ./package-lock.json package-lock.json
RUN npm i

COPY . .

ENV INLINE_RUNTIME_CHUNK=false
RUN npm run build


COPY ./configs/nginx.conf /etc/nginx/nginx.conf
COPY ./configs/default.conf /etc/nginx/conf.d/default.conf
COPY ./configs/cmd.sh /root/cmd.sh
RUN cp -r /build /usr/share/nginx/html/portal
COPY ./configs/redirect.html /usr/share/nginx/html/index.html

HEALTHCHECK --interval=30s --timeout=3s \
        CMD curl -f http://localhost/ || exit 1

EXPOSE 8080

CMD ["node", "customServer.js"]
