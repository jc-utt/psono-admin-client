const express = require("express")
const app = express()
app.use("/portal", express.static("build"))
app.listen(8080, () => console.log("listening"))
